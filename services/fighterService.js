const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    // TODO: Implement methods to work with fighters
    getFighter(param) {
        return FighterRepository.getOne(param);
    }
    createNewFighter(data){
        return FighterRepository.create(data);
    }
    updateFighter(fighterId, newData){
        return FighterRepository.update(fighterId, newData);
    }
    deleteFighter(fighterId) {
        return FighterRepository.delete(fighterId);
    }
    deleteAllFighters() {
        while(FighterRepository.getAll().length > 0) {
            this.deleteFighter(FighterRepository.getAll()[0]['id']);
        }
    }
    getAllFighters() {
        return FighterRepository.getAll();
    }
}

module.exports = new FighterService();