const { FightRepository } = require('../repositories/fightRepository');

class FightersService {
    // OPTIONAL TODO: Implement methods to work with fights
    createNewFights(data) {
        return FightRepository.create(data);
    }
}

module.exports = new FightersService();