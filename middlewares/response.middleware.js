const UserService = require('../services/userService');
const FighterService = require('../services/fighterService');

const responseMiddleware = (req, res, next) => {
   // TODO: Implement middleware that returns result of the query
   
   if(UserService.getUser({email: req.body.email})) {
       res.status(400).json({
           error: true,
           message: 'such user already exists'
       });
       return 'error';
   }
   if(UserService.getUser({phoneNumber: req.body.phoneNumber})) {
       res.status(400).json({
           error: true,
           message: 'such user already exists'
       });
       return 'error';
   }
   if(FighterService.getFighter({name: req.body.name})) {
       res.status(400).json({
           error: true,
           message: 'such fighter already exists'
       });
       return 'error';
   }
    next();
}

exports.responseMiddleware = responseMiddleware;