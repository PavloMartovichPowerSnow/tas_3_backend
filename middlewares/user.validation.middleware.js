const { user } = require('../models/user');
const createUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during creation
    if(req.body.email === undefined ||
       req.body.phoneNumber === undefined ||
       req.body.firstName === undefined ||
       req.body.lastName === undefined ||
       req.body.password === undefined) {
        res.status(400).json({
            error: true,
            message: 'empty field not allowed'
        });
        return 'error';
    }
    if(req.body.email
        .slice(req.body.email.length - 10) !== '@gmail.com' ||
        req.body.email.length < 11) {
        res.status(400).json({
            error: true,
            message: 'invalid email'
        });
        return 'error';
    }
    if(req.body.phoneNumber.slice(0,4) !== '+380' ||
       req.body.phoneNumber.length !== 13 ||
      !req.body.phoneNumber.split('').every(e => {
           return "+1234567890".includes(e);
       })) {
        res.status(400).json({
            error: true,
            message: 'invalid phone number'
        });
        return 'error';
    }
    if(req.body.password.length < 3) {
        res.status(400).json({
            error: true,
            message: 'password too short'
        });
        return 'error';
    }
    
    next();
}

const updateUserValid = (req, res, next) => {
    // TODO: Implement validatior for user entity during update

    const params = {
        firstName: '',
        lastName: '',
        password: ''
    }

    if(!Object.keys(req.query).every(e => {
        return Object.keys(params).includes(e);
    })) {
        res.status(400).json({
            error: true,
            message: 'property not found'
        });
        return 'error';
    }
    if(!Object.values(req.query).every(e => {
        return e.length > 0;
    })) {
        res.status(400).json({
            error: true,
            message: 'empty values not allowed'
        });
        return 'error'
    }
    next();
}

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;